/*
 * IOT scheduler template
 * The purpose of this scheduler is to implement a simple scheduler named IoT scheduler.
 * scheduler that outperforms CFS scheduler by asserting real-time priority when running the surveillance system in a busy environment.
 * The template here is for the students to implement the correct source code for the IoT scheduler.
 * For this assignment, the students are expected to modify
 *
 * 1. select_task_rq_iot
 * 2. enqueue_task_iot
 * 3. dequeue_task_iot
 * 4. pick_next_task_iot
 * 5. rq_online_iot
 * 6. rq_offline_iot
 *
 * Please refer to the FIXME: lines where lines briefly explain what implementation the scheduler expects as a minimum requirement.
 *
 * Regarding to implementing other functions.
 * The basic requirement is to have the basic scheduler working without task preemption and load-balancing while running on multiple CPUs.
 * However, students are encouraged to add the preemption and load-balancing if the time is alloweded
 */

/*
 * basic structures used in the scheduler
 * const struct sched_class iot_sched_class: a scheduling class is a set of function pointers, defined through struct sched_class
 * struct iot_rq: run queue of iot scheduler
 * struct sched_iot_entity *iot_se: a minimal schedulable entity that contains the addresses and attributes required to run iot scheduler
 */

#include "sched.h"
#include <linux/slab.h>
#include <linux/irq_work.h>

static DEFINE_PER_CPU(cpumask_var_t, local_cpu_mask);

/* Initialize your scheduler specific runqueue */
void init_iot_rq(struct iot_rq *iot_rq)
{
    /*
     * This function is run once per-cpu. It can be
     * used to initalize a runqueue specific to your
     * scheduler class.
     *
     * i.e. Initaliaze variables, entites, variables, etc.
     */

    struct iot_prio_array *array;
    int i;

    // Get the address of the active run-queues
    array = &iot_rq->active;

    // Initialize bitmap
    for (i = 0; i < MAX_IOT_PRIO; i++) {
        INIT_LIST_HEAD(array->queue + i);
        __clear_bit(i, array->bitmap);
    }

    /* delimiter for bitsearch: */
    __set_bit(MAX_IOT_PRIO, array->bitmap);

    // Set highest priority value
    iot_rq->highest_prio.curr = MAX_IOT_PRIO;

    /* We start in dequeued state, because no IOT tasks are queued */
    iot_rq->iot_queued = 0;
}

/* Initialize scheduler class */
void __init init_sched_iot_class(void)
{
   /*
    * This function is called to initialize your scheduler class. This will only
    * be called once, not per-CPU. Here you can initialize any variables, masks,
    * priorities, etc.
    */

    // clear all cpus (< nr_cpu_ids) in a cpumask
    // cpumasks provide a bitmap suitable for representing the
    // set of CPU's in a system, one bit position per CPU number.
    unsigned int i;
    for_each_possible_cpu(i) {
        zalloc_cpumask_var_node(&per_cpu(local_cpu_mask, i),GFP_KERNEL, cpu_to_node(i));
    }
}

/* [required to implement]  Add task to runqueue */
static void enqueue_task_iot(struct rq *rq, struct task_struct *p, int flags)
{

     	// FIXME: add the entity to the run queue
	struct sched_iot_entity *iot_se = &p->iot_t;
	// the entity is contained within the task given

	//if (flags & ENQUEUE_WAKEUP) rt_se->timeout = 0;
	struct iot_rq *iot_rq = &rq->iot_rq; //get the iot run queue

	struct iot_prio_array *array = &iot_rq->active;
	// the iot_prio_array is containted within the iot_rq

	struct list_head *queue = array->queue + p->prio;
	// access the subqueue (linked list) of the priority queue. Hence why we
	// add the priority as it represents its index on the priority array to 
	// the array queue

	//if (move_entity(flags)){
	WARN_ON_ONCE(iot_se->on_list);
	if (flags & ENQUEUE_HEAD) list_add(&iot_se->run_list, queue);
	// priority array subqueue == run queue == ready queue 
	// we want to add the run_list to the subqueue (queue) to get ready to run

	else list_add_tail(&iot_se->run_list, queue);
	// add it to the tail if it is already initialized

     	// FIXME: update the bitmap of the priority array
	__set_bit(p->prio, array->bitmap);
	// set the bit in the bitmap correspoding to the index in the priority queue

	iot_se->on_list = 1;
	// set the iot entity on_list variable to 1 indicating we have already enqueued
	// the task structure.
	//}

	iot_se->on_rq = 1;
	// set the iot entity on_rq variable to 1 indicating we have set the task on the
	// run queue


    	// FIXME: set the total number of running tasks in struct iot_rq
	iot_rq->iot_nr_running += 1;
	// increment the number of running tasks in the iot run queue

     	// FIXME: set the total number of running tasks in struct rq 
	rq->nr_running += 1;
	// increment the number of running tasks in the global runqueue (keep iot run queue
	// and global run queue synchronized)
	
	if (p->prio < iot_rq->highest_prio.curr) iot_rq->highest_prio.curr = p->prio;

}

/* [required to implement] Remove task from runqueue */
static void dequeue_task_iot(struct rq *rq, struct task_struct *p, int flags)
{

	struct sched_iot_entity *iot_se = &p->iot_t;
	struct iot_rq *iot_rq = &rq->iot_rq;
	struct iot_prio_array *array = &iot_rq->active;

	//if (move_entity(flags)){
	WARN_ON_ONCE(!iot_se->on_list);
	list_del_init(&iot_se->run_list);
	if (list_empty(array->queue + p->prio)) __clear_bit(p->prio, array->bitmap);
	iot_se->on_list = 0;
	//}
	iot_se->on_rq = 0;
	
	
     	// FIXME: decrement the total number of running queues in struct iot_rq
	iot_rq->iot_nr_running -= 1;
	// decrement the number of running tasks in the iot run queue

	rq->nr_running -= 1;
	// decrement the number of running tasks in the global runqueue (keep iot run queue
	// and global run queue synchronized)

     // FIXME: iterate through the struct iot_entities and find the entity to remove
     // FIXME: set the status of the struct sched_iot_entity to be dequeued
     // FIXME: remove sched_iot_entity from the list
	if (iot_rq->iot_nr_running){
		if (p->prio == iot_rq->highest_prio.curr){
			iot_rq->highest_prio.curr = sched_find_first_bit(array->bitmap);
		}
	} else iot_rq->highest_prio.curr = MAX_IOT_PRIO;
}


/* [required to implement] Specify on which CPU the process should run */
static int select_task_rq_iot(struct task_struct *p, int cpu, int sd_flag, int flags)
{
    return cpu;
}

/*  [required to implement] Choose the task to be run next */
static struct task_struct *pick_next_task_iot(struct rq *rq, struct task_struct *prev, struct rq_flags *rf)
{

	// FIXME: get the queue of the current priority
	put_prev_task(rq, prev);
	struct task_struct *p;
	struct iot_rq *iot_rq = &rq->iot_rq; //get the iot run queue
	struct sched_iot_entity *iot_se;
	struct iot_prio_array *array = &iot_rq->active;

	int idx = sched_find_first_bit(array->bitmap);
	//BUG_ON(idx >= MAX_RT_PRIO);
	if (idx < MAX_RT_PRIO){
		struct list_head *queue = array->queue + idx;

	     // FIXME: select the struct sched_iot_entity from the queue
		iot_se = list_entry(queue->next, struct sched_iot_entity, run_list);

	     // FIXME: return the task_struct of the sched_iot_entity
		p = container_of(iot_se, struct task_struct, iot_t);
		p->se.exec_start = rq_clock_task(rq);
		return p;
	}
	else{
		return NULL;
	}
}

/* [required to implement] Assume rq->lock is held */
static void rq_online_iot(struct rq *rq)
{
    /*
     * This function puts the runqueue online, if it is not already. The
     * function is called:
     *  1) In the early boot process
     *  2) At runtime, if cpuset_cpu_active() fails to rebuild the domains
     *
     * In basic terms: A cpuset is a mechanism that allows one to assign a
     * set of CPUs and Memory to a set of tasks.
     *
     * You can use this function to maintain stats or set masks.
     * i.e. CPU priority, mask, etc.
     */

     // FIXME: Update the CPU priority settings of a task in the run queue, changing the priority to the highest of in the run queue.
	cpupri_set(&rq->rd->cpupri, rq->cpu, rq->iot_rq.highest_prio.curr);
}

/* [required to implement] Assume rq->lock is held */
static void rq_offline_iot(struct rq *rq)
{
    /*
     * This function is called in the event the runqueue is being taken offline.
     * You can free up any resources belonging to the specific CPU runqueue being
     * passed. i.e. any stats or masks being held for the CPU or runqueue
     */

     // FIXME: Update the CPU priority settings of a task in the run queue by changing the task priority to invalid.
	cpupri_set(&rq->rd->cpupri, rq->cpu, CPUPRI_INVALID);
}

/* Preempt the current task with a newly woken task, if needed */
static void check_preempt_curr_iot(struct rq *rq, struct task_struct *p, int flags)
{
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2
    
    /*
     * In this function, you will be choosing what do when a new
     * task has become runnable. Mainly, you will decide whether
     * this new task will preempt the task that is currently
     * running.
     */

    /*
     *  wake flags
     *
     *  WF_SYNC     -   waker goes to sleep after wakeup
     *  WF_FORK     -   child wakeup after fork
     *  WF_MIGRATED -   internal use, task got migrated
     */
}

/* Task woken up */
static void task_woken_iot(struct rq *rq, struct task_struct *p){
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2

    /*
     * This function is called when a task is woken up. The task is
     * not currently running and has not been called to be scheduled.
     */
}

/* Set the current task */
static void set_curr_task_iot(struct rq *rq) {
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2
    
    /*
     * This function is mainly called when a task is changing either its
     * policy or group. This function can be used to keep various stats
     * or housekeeping items. Dependant on the implementation of your
     * scheduler.
     */
}

static void update_curr_iot(struct rq *rq) {
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2
    
    /*
     * Here you can update any information on the current task relevant to
     * your scheduler. i.e. runtime statistics
     */
}

/* Task has switched from our scheduler class */
static void switched_from_iot(struct rq *rq, struct task_struct *p){
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2

    /*
     * This function is called when a task has switched scheduler classes
     * and is no longer assigned to our scheduler class. Here you can do
     * some housekeeping relevant to the task switched from our scheduler.
     */
}

/* Priority of a task has changed */
static void prio_changed_iot(struct rq *rq, struct task_struct *p, int oldprio) {
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2

    /*
     * This function is called when a task has switched priority values. Here you can
     * perform any relevant functions specific to your scheduler implementation. You
     * are also given the previous priority value of the task.
     */
}

/* Function called on timer interrupt */
static void task_tick_iot(struct rq *rq, struct task_struct *p, int queued) {
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2

    /*
     * This function is called whenever there is an interrupt from the timer.
     * This allows you to perform functions periodically on your scheduler, as
     * other functions may be called only under certain pre-defined events. You
     * can use this functionality to update various stats pertaining to your
     * scheduler, reschedule a task, maintain any data structures, etc.
     */
}

/* Task has switched to our scheduler class */
static void switched_to_iot(struct rq *rq, struct task_struct *p) {
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2
    
    /*
     * This function is classed when a task task has been assigned our scheduler
     * class. The task is part of our scheduler class. Here you can perform
     * functions that are relevant to your scheduler implementation.
     */
}

/* Task switched out of CPU */
static void put_prev_task_iot(struct rq *rq, struct task_struct *p) {
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2
    
    /*
     * This function is called whenever a task is switched out of the
     * CPU. The function that will be running next is chosen by
     * pick_next_task(). You can choose to perform various upkeeping
     * and functions during the task switching process both here and
     * pick_next_task(), or all in pick_next_task(). Again, this will
     * be a decision based upon your scheduler class implementation.
     */
}

/* Give up CPU */
static void yield_task_iot(struct rq *rq) {
    
    // Note that this function is not strictly required for you to implement the basic scheduler for Project 2-2
   
    /*
     * This function is called whenever a process voluntarily makes a call
     * to give up CPU. Specifically, this is done via system call by a
     * user-space process.
     */
}

/* Scheduler class functions */
const struct sched_class iot_sched_class = {
        .next               = &fair_sched_class,
        .enqueue_task       = enqueue_task_iot,
        .dequeue_task       = dequeue_task_iot,
        .yield_task         = yield_task_iot,
        .check_preempt_curr = check_preempt_curr_iot,
        .pick_next_task     = pick_next_task_iot,
        .put_prev_task      = put_prev_task_iot,
        .select_task_rq     = select_task_rq_iot,
        .set_cpus_allowed   = set_cpus_allowed_common,
        .rq_online          = rq_online_iot,
        .rq_offline         = rq_offline_iot,
        .task_woken         = task_woken_iot,
        .switched_from      = switched_from_iot,
        .set_curr_task      = set_curr_task_iot,
        .task_tick          = task_tick_iot,
        .prio_changed       = prio_changed_iot,
        .switched_to        = switched_to_iot,
        .update_curr        = update_curr_iot,
};
