#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/sched.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/sched/signal.h>

static int pid = -1;
module_param(pid, int, 0);
MODULE_PARM_DESC(pid, "The process ID to search");


int memoryModule_init(void)
{
    struct task_struct *task;
    struct mm_struct *mm = NULL;
    struct vm_area_struct *vmaList = NULL;
    pgd_t *global;
    p4d_t *fourth;
    pud_t *upper;
    pmd_t *middle;
    pte_t *table;
    int present = 0;
    int swap = 0;
    unsigned long i;
    unsigned long pg_addr_inc = PAGE_SIZE;
    if (pid == -1) return 0;
    printk(KERN_INFO "pid = %d\n", pid);
    for_each_process(task)
    {
        if(task->pid == pid)
        {
            mm = task->mm;
            vmaList= mm->mmap;
        }
    }
    
    if (mm == NULL) return 0;
    
    down_read(&mm->mmap_sem);

    while(vmaList != NULL)
    {
        for(i = vmaList->vm_start; i < vmaList->vm_end; i += pg_addr_inc)
        {
            global = pgd_offset(mm, i);
            if (!pgd_none(*global) && likely(!pgd_bad(*global)))
            {
                fourth = p4d_offset(global, i);
                if (!p4d_none(*fourth) && likely(!p4d_bad(*fourth)))
                {
                    upper = pud_offset(fourth, i);
                    if (!pud_none(*upper) && likely(!pud_bad(*upper)))
                    {
                        middle = pmd_offset(upper, i);
                        if (!pmd_none(*middle) && likely(!pmd_bad(*middle)))
                        {
                            table = pte_offset_map(middle, i);
                            if(pte_present(*table)) ++present; //check if page is in memory
                            else if (pte_none(*table) == 0) ++swap; //check to make page is non-zero [is valid]
                        }
                    }
                }
            }
        }   // end of for loop

        vmaList = vmaList->vm_next;

    }   // end of while loop
    up_read(&mm->mmap_sem);

    printk(KERN_INFO "PRESENT: %u  SWAPPED: %u ", present , swap);
    return 0;
}
void memoryModule_exit(void)
{
    printk(KERN_INFO "Goodbye!\n");
}


// We need at least two functions in a module, init and exit. Init function is called when the 
// module is loaded (insmod) into the kernel, and exit function is called when the module is 
// removed from the kernel (rmmod). 

module_init(memoryModule_init);             // defines the printMemory_init to be called at module loading time
module_exit(memoryModule_exit);           // defines the printMemory_exit to be called at module unload time
