#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/sched.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/sched/signal.h>
#include <linux/hrtimer.h>
#include <linux/ktime.h>
//#include <unistd.h>


//unsigned long timer_interval_ns = 5e9;
static struct hrtimer hr_timer;

void checkSize(void);

enum hrtimer_restart timer_callback( struct hrtimer *timer_for_restart )
{
    ktime_t currtime , interval;
    currtime  = ktime_get();
    interval = ktime_set(5,0);
    hrtimer_forward(timer_for_restart, currtime , interval);
    checkSize();
    return HRTIMER_RESTART;
}

int memoryModule_init(void)
{
    printk(KERN_INFO "[PID]: [WSS]");
    ktime_t ktime;
    ktime = ktime_set(5,0);
    hrtimer_init( &hr_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL );
    hr_timer.function = &timer_callback;
    hrtimer_start( &hr_timer, ktime, HRTIMER_MODE_REL );
    return 0;
}

void memoryModule_exit(void)
{
    printk(KERN_INFO "Goodbye!\n");
    int ret;
    ret = hrtimer_cancel( &hr_timer );
    if (ret) printk("Timer was still in use!\n");
}

void checkSize(void){
    struct task_struct *task;
    struct mm_struct *mm = NULL;
    struct vm_area_struct *vmaList = NULL;
    pgd_t *global;
    p4d_t *fourth;
    pud_t *upper;
    pmd_t *middle;
    pte_t *table;
    unsigned int twss = 0;
    unsigned long i;
    unsigned long pg_addr_inc = PAGE_SIZE;
    
    
    for_each_process(task)
    {
        mm = task->mm;
        if (mm != NULL)
        {
            down_read(&mm->mmap_sem);
            vmaList= mm->mmap;
            while(vmaList != NULL)
            {
                for(i = vmaList->vm_start; i < vmaList->vm_end; i += pg_addr_inc)
                {
                    global = pgd_offset(mm, i);
                    if (!pgd_none(*global) && likely(!pgd_bad(*global)))
                    {
                        fourth = p4d_offset(global, i);
                        if (!p4d_none(*fourth) && likely(!p4d_bad(*fourth)))
                        {
                            upper = pud_offset(fourth, i);
                            if (!pud_none(*upper) && likely(!pud_bad(*upper)))
                            {
                                middle = pmd_offset(upper, i);
                                if (!pmd_none(*middle) && likely(!pmd_bad(*middle)))
                                {
                                    table = pte_offset_map(middle, i);
                                    if(pte_young(*table))
                                    {
                                        ++twss;
                                        pte_mkold(*table);
                                    }
                                }
                            }
                        }
                    }
                }   // end of for loop
                vmaList = vmaList->vm_next;
                
            }   // end of while loop
            up_read(&mm->mmap_sem);
        }
        
    }
    // getting the physical memory size
    //in pages = 224 166
	//90 percent = 201 750
    // total wss is compared to 90% of total physical space
    if(twss > (201750))
    {
        printk(KERN_INFO "Kernel Alert!");
    }
    
    printk(KERN_INFO "[TWSS] : [%u]", twss);
}
    
    

module_init(memoryModule_init);             // defines the printMemory_init to be called at module loading time
module_exit(memoryModule_exit);           // defines the printMemory_exit to be called at module unload time
MODULE_LICENSE("GPL");
