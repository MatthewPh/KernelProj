#!/bin/sh
export KERNEL=kernel7
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- bcm2709_defconfig
make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- zImage modules dtbs -j 2
sudo umount /media/matthew/rootfs
sudo umount /media/matthew/boot
sudo mount /dev/mmcblk0p1 /mnt/boot
sudo mount /dev/mmcblk0p2 /mnt/root
sudo make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- INSTALL_MOD_PATH=/mnt/root modules_install
sudo cp arch/arm/boot/zImage /mnt/boot/kernel7.img
sudo cp arch/arm/boot/dts/*.dtb /mnt/boot/
sudo cp arch/arm/boot/dts/overlays/*.dtb* /mnt/boot/overlays/
sudo cp arch/arm/boot/dts/overlays/README /mnt/boot/overlays/
sudo umount /mnt/boot
sudo umount /mnt/root
