#include<linux/kernel.h>
#include<linux/init.h>
#include<linux/sched.h>
#include<linux/sched/signal.h> //in case the for each process changed places
#include<linux/syscalls.h>
#include "mycall.h"
#include <linux/slab.h>  //added for kmalloc


SYSCALL_DEFINE4(get_user_info, int, uid, struct procList**, user_space, int*, allocatedObjects, struct procList*, kernel_space){
	//Utilize the SYSCALL_DEFINEx macro to allow multiple inputs for our syscall.	
	
	struct procList* head = NULL; //the head of the procList 
	struct procList* temp = NULL; //setup an intermediate pointer for procList
	int i = 0;

	//macro where we pass in our task parameter
	if ((*user_space) == NULL){
	    struct task_struct *task; //create a task struct to loop
	    int object_count = 0;
	    int first = 0; //define if we are inserting the first element or not
	    int taskId = 0; //taskId of the process
    	for_each_process(task){
    		taskId = (int) task->cred->uid.val;
    		if (taskId == uid){ //the userId of the task matches the user nameId passed in
    
//    			printk("REACHES UID\n");
    
    			if (first == 0){ //the first element of the ll we want to insert
  //  			printk("REACHES first == 0\n");
    
    
    				head = kmalloc(sizeof(struct procList), GFP_USER);
    				
    					if (head == NULL){
    						return -1; //no more memory to allocate
    					}
    					
    				head->PID = task->pid;
    				
    //				printk("%d PID VALUE\n", head->PID);
    
    
    				//insert into the char array
    				for(i=0; i<16; i++){
    				    head->COM[i] = task->comm[i];
    				}
    				
    				head->PRI = task->prio;
    				
    				head->CLS = task->policy;         //possible way?
    
    				
    				head->next = kmalloc(sizeof(struct procList), GFP_USER);
    				temp = head->next; 
    					if (temp == NULL){
    						kfree(head);
    						return -1; //no more memory to allocate
    					}
    				first = 1;
    
    //			printk("EXIT first == 0\n");
    			
                object_count = 1;
    			}
    			else if (first == 1){ //the subsequent elements of the ll we want to insert
    			
    //			printk("REACHES first == 1\n");
    
    				temp->PID = task->pid;
    				
    //				printk("%d PID VALUE\n", temp->PID);
    
    				//insert into the char array
    				for(i=0; i<16; i++){
    				    temp->COM[i] = task->comm[i];
    				}
    				
    				temp->PRI = task->prio;
    
    				temp->CLS = task->policy;         //possible way?
    
    				
    				first = 2;
    //			printk("EXIT first == 1\n");
    			
                object_count = 2;
    			}
    			else{
    
    //			printk("REACHES else STATEMENT\n");
    
    				temp->next = kmalloc(sizeof(struct procList), GFP_USER);
    					if (temp->next == NULL){
    						while(head != NULL){
    							temp = head->next;
    							kfree(head);
    							head = temp;
    						}
    						return -1; //no more memory to allocate
    					}
    				temp = temp->next;
    				
    				temp->PID = task->pid;
    				
    //				printk("%d PID VALUE\n", temp->PID);
    
    				//insert into the char array
    				for(i=0; i<16; i++){
    				    temp->COM[i] = task->comm[i];
    				}
    				temp->PRI = task->prio;
    
    				temp->CLS = task->policy;         //possible way?
    
    //			printk("EXIT else STATEMENT\n");
    			
                ++object_count;
    				
    			}
    		}
    	}
    //	printk("FINISHES FOREACH\n");
    //	printk("%d \n", head);
    	temp->next = NULL;
    	put_user(head, user_space);
    	put_user(object_count, allocatedObjects);
	}
	else{
	    struct procList* head_next;
	    struct procList* temp_next;
	    int j = (*allocatedObjects);
	    head = *user_space;
	//	printk("%d This is head\n", head);
        head_next = head->next;
	//	printk("%d This is head_next\n", head_next);

	    temp = kernel_space;
		
	    while(j != 0 && temp != NULL){
	        i = copy_to_user((void*)head, (void*)temp, sizeof(struct procList));
            if (i == 0){ //means copy_to_user was successful
 //               printk("ENTER THE ONION\n");
/*
                printk("%d This is head PID\n", (int) head->PID);
				printk("%s This is head COM\n", head->COM);
				printk("%d This is head PRI\n", head->PRI);
				printk("%d This is head CLS\n", head->CLS);


	        printk("%d This is temp PID\n", (int) temp->PID);
			printk("%s This is temp COM\n", temp->COM);
			printk("%d This is temp PRI\n", temp->PRI);
			printk("%d This is temp CLS\n", temp->CLS);	    
*/
				head->next = head_next;
			    head = head->next;
			    head_next = head->next;
	//			printk("%d\n", head);
                temp_next = temp->next;
                kfree(temp);
                temp = temp_next;
            }
            else{
//				printk("ENTER YOUR MUM\n");
                while(temp != NULL){
                    temp_next = temp->next;
                    kfree(temp);
                    temp = temp_next;
                }
                return -1;
            }
	        --j;
			
	    }
	}
//	printk("FINISHES SYSCALL\n");
	return 0;
}
