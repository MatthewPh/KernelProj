typedef struct procList{
	pid_t PID;
	char COM[16]; 
	//16 is the maximum length of a command specified
	//as specified by the TASK_COMM_LEN macro in sched.h
	int PRI;
	int CLS;
	struct procList *next;
};
asmlinkage long sys_get_user_info(int uid, struct procList **user_space, int *allocatedObjects, struct procList *kernel_space);